package com.example.appsport;

import com.example.appsport.Model.Blogs;
import com.example.appsport.Model.Exercises;
import com.example.appsport.Model.Rutinas;

public interface iBridgeFragments {
    public void sendExer(Exercises exer);
    public void sendBlogs(Blogs blog);
    public void Exercices(Rutinas rut);
}
