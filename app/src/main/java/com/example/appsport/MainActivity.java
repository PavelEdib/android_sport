package com.example.appsport;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;


import com.example.appsport.Fragments.FragmentAllExer;
import com.example.appsport.Fragments.FragmentBlogs;
import com.example.appsport.Fragments.FragmentDetailsBlog;
import com.example.appsport.Fragments.FragmentDetailsExercises;
import com.example.appsport.Fragments.FragmentMyExer;
import com.example.appsport.Model.Blogs;
import com.example.appsport.Model.Exercises;
import com.example.appsport.Model.Rutinas;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity  implements  NavigationView.OnNavigationItemSelectedListener,iBridgeFragments{
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    Toolbar toolbar;
    NavigationView navigationView;

    //Variable for Fragments
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    //variable for Fragment DetailExercises
    FragmentDetailsExercises fragmentDetailsExercises;
    //variable for Fragment DetailBlogs
    FragmentDetailsBlog fragmentDetailsBlog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar =findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.drawer);
        navigationView = findViewById(R.id.navigationView);

        //Set event OnClick navigationview
        navigationView.setNavigationItemSelectedListener(this);

        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.open,R.string.close);

        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        actionBarDrawerToggle.syncState();

        //Load Fragment Blog's
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, new FragmentBlogs());
        fragmentTransaction.commit();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        drawerLayout.closeDrawer(GravityCompat.START);
        if(menuItem.getItemId()==R.id.blogs){
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container, new FragmentBlogs());
            fragmentTransaction.commit();
        }
        if(menuItem.getItemId()==R.id.Exer){
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container, new FragmentAllExer());
            fragmentTransaction.commit();
        }
        if(menuItem.getItemId()==R.id.MyExer){
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container, new FragmentMyExer(null));
            fragmentTransaction.commit();
        }

        if(menuItem.getItemId()==R.id.logout){
            Dialog_LogOut();
        }

        if(menuItem.getItemId()==R.id.exit){
            Dialog_Exit();
        }

        return false;
    }


    public void Dialog_LogOut( ){

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Log Out!");
        builder.setMessage("Do you want to Log out?");
        builder.setPositiveButton("Yes. Log Out now!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i){
                finish();
            }
        });

        builder.setNegativeButton("Not now", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i){
                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void Dialog_Exit( ){

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Close Application!");
        builder.setMessage("Do you want to Exit from Application?");
        builder.setPositiveButton("Yes. Exit now!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i){
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);
            }
        });

        builder.setNegativeButton("Not now", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i){
                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void sendExer(Exercises exer) {
        fragmentDetailsExercises = new FragmentDetailsExercises();
        //object bundle to transport the information
        Bundle bundleSend = new Bundle();
        //Send the object of type Serializable
        bundleSend.putSerializable("object",exer);
        fragmentDetailsExercises.setArguments(bundleSend);
        //open the Fragment
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragmentDetailsExercises);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void sendBlogs(Blogs blog) {
        fragmentDetailsBlog = new FragmentDetailsBlog();
        //object bundle to transport the information
        Bundle bundleSend = new Bundle();
        //Send the object of type Serializable
        bundleSend.putSerializable("objectBlog",blog);
        fragmentDetailsBlog.setArguments(bundleSend);
        //open the Fragment
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragmentDetailsBlog);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void Exercices(Rutinas rutinas) {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, new FragmentMyExer(rutinas.getId()));
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


}