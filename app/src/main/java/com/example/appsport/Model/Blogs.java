package com.example.appsport.Model;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appsport.Fragments.FragmentBlogs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

public class Blogs implements Serializable {
    private String title;
    private String img_id;
    private String content;


    public Blogs(String title, String img_id,String content) {
        this.title = title;
        this.img_id = img_id;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public String getImg_id() {
        return img_id;
    }

    public String getContent() { return this.content; }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setImg_id(String img_id) {
        this.img_id = img_id;
    }

    public void setDate(String date) { this.content = date;  }


    public static void getAllBlogs(final Context context){
        String URL ="http://www.powerplayall.com/Android/blogs.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(!response.isEmpty()){
                    try {
                        getArrayFromJson( new JSONArray(response.trim()),context);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Log.e("Blogs:" , "Nothing");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return null;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }


    private static void getArrayFromJson(JSONArray json,Context context){
        ArrayList<Blogs> list_blog = new ArrayList<>();
        for(int i=0;i<json.length();i++){
            try {
                JSONObject obj = json.getJSONObject(i);
                list_blog.add( new Blogs(
                        obj.getString("title"),
                        "http://www.powerplayall.com/ad/uploads/"+obj.getString("Image_path"),
                        obj.getString("content")
                ));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        FragmentBlogs.ShowDates(list_blog,context);

    }



}
