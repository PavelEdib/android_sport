package com.example.appsport.Model;

import com.example.appsport.R;
import java.util.ArrayList;

public class Rutinas{
    private int id;
    private String title;
    private int img;
    private ArrayList<Exercises> list_exer;



    public Rutinas(int id, String title) {
        this.id = id;
        this.title = title;
        this.img = R.drawable.ic_myexer;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public int getImg() {
        return this.img;
    }

    public int getId() {  return id;  }

    public void setId(int id) { this.id = id; }




}
