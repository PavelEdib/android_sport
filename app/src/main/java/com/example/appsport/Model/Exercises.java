package com.example.appsport.Model;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;



import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appsport.Fragments.FragmentMyExer;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Exercises  implements Serializable {
    private String name;
    private String img_id;


    public  Exercises(){

    }
    public Exercises(String name, String img_id) {
        this.name = name;
        this.img_id = img_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImg_id(String img_id) {
        this.img_id = img_id;
    }

    public String getName() {
        return name;
    }

    public String getImg_id() {
        return img_id;
    }

    public static void getAllExercicios(final Context context){

        String URL = "http://www.powerplayall.com/Android/exercicios.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(!response.isEmpty()){
                    try {
                        getArrayFromJson( new JSONArray(response.trim()),context);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Log.e("Exercicios:" , "Nothing");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                return null;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);

    }


    public static void exerById(int id_rutina, final Context context){
        final int id = id_rutina;
        final ArrayList<Exercises> arrayList = new ArrayList<>();
        String URL = "http://www.powerplayall.com/Android/Exer_Rut.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(!response.isEmpty()){
                    try {
                        getArrayFromJson( new JSONArray(response.trim()),context);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Log.e("Exercicios:" , "Nothing");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_rutina",id+"");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);

    }



    private static void getArrayFromJson(JSONArray json,Context context){
          ArrayList<Exercises> list_exer = new ArrayList<>();
        for(int i=0;i<json.length();i++){
            try {
                JSONObject obj = json.getJSONObject(i);
                list_exer.add( new Exercises(
                        obj.getString("nombre"),
                        "http://www.powerplayall.com/ad/uploads/Exer/"+obj.getString("Image_path")
                ));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

         FragmentMyExer.ShowDates(list_exer,context);


    }

}
