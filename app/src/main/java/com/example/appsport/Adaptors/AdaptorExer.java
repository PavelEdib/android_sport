package com.example.appsport.Adaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.appsport.Model.Exercises;
import com.example.appsport.R;
import com.example.appsport.Utils.DownloadImageTask;

import java.util.ArrayList;

public class AdaptorExer extends RecyclerView.Adapter<AdaptorExer.ViewHolder> implements View.OnClickListener {

    LayoutInflater inflater;

    //listener
    private View.OnClickListener listener;

    ArrayList<Exercises> Model_Exer;

    //constructor de Adaptor
    public AdaptorExer(Context context,ArrayList<Exercises> model){
        this.inflater = LayoutInflater.from(context);
        this.Model_Exer = model;
    }

    //instanciamos los el listener
    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_exercice,parent,false);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String Name = Model_Exer.get(position).getName();
        String url = Model_Exer.get(position).getImg_id();

        holder.name.setText(Name);
        //holder.img.setImageResource(imgId);
        new DownloadImageTask(holder.img).execute(url);
    }

    @Override
    public int getItemCount() {
        return Model_Exer.size();
    }

    @Override
    public void onClick(View v) {
        if(listener!=null){
            listener.onClick(v);
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView name;
        ImageView img;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.nameExer);
            img = itemView.findViewById(R.id.img_Exer);
        }
    }
}
