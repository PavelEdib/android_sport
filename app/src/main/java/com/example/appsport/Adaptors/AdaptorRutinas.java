package com.example.appsport.Adaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.appsport.Model.Rutinas;
import com.example.appsport.R;

import java.util.ArrayList;

public class AdaptorRutinas extends RecyclerView.Adapter<AdaptorRutinas.ViewHolder> implements View.OnClickListener{
    LayoutInflater inflater;

    private View.OnClickListener listener;

    ArrayList<Rutinas> list_rutinas;


    public AdaptorRutinas(Context context, ArrayList<Rutinas> list){
        this.inflater = LayoutInflater.from(context);
        this.list_rutinas = list;
    }

    public void setOnClickListener(View.OnClickListener listener){this.listener = listener;}

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_rutinas,parent,false);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String title = list_rutinas.get(position).getTitle();
        int img = list_rutinas.get(position).getImg();

        holder.title.setText(title);
        holder.img.setImageResource(img);
    }

    @Override
    public int getItemCount() {
        return this.list_rutinas.size();
    }

    @Override
    public void onClick(View v) {
        if(this.listener!=null){
            listener.onClick(v);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView title;
        ImageView img;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.titleRutinas);
            img = itemView.findViewById(R.id.img_Rut);
        }
    }
}
