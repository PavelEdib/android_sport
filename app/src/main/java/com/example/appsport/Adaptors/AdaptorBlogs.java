package com.example.appsport.Adaptors;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.appsport.Model.Blogs;
import com.example.appsport.Model.Exercises;
import com.example.appsport.R;
import com.example.appsport.Utils.DownloadImageTask;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class AdaptorBlogs extends RecyclerView.Adapter<AdaptorBlogs.ViewHolder> implements View.OnClickListener{

    LayoutInflater inflater;
    //listener
    private View.OnClickListener listener;

    ArrayList<Blogs> Model_Blogs;

    public AdaptorBlogs(Context context, ArrayList<Blogs> model){
        this.inflater = LayoutInflater.from(context);
        this.Model_Blogs = model;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_blogs,parent,false);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String title = Model_Blogs.get(position).getTitle();
        //String content = Model_Blogs.get(position).getContent();
        String imgUrl = Model_Blogs.get(position).getImg_id();


        holder.title.setText(title);
        holder.date.setText("14-12-2021");
        //holder.img.setImageBitmap(bmp);
        new DownloadImageTask(holder.img).execute(imgUrl);
    }

    @Override
    public int getItemCount() { return Model_Blogs.size(); }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if(listener!=null){
            listener.onClick(v);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView title,date;
        ImageView img;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.nameBlog);
            img = itemView.findViewById(R.id.img_Blog);
            date = itemView.findViewById(R.id.Creation_date);
        }
    }

}
