package com.example.appsport.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appsport.Adaptors.AdaptorBlogs;
import com.example.appsport.Adaptors.AdaptorRutinas;
import com.example.appsport.Model.Exercises;
import com.example.appsport.Model.Rutinas;
import com.example.appsport.R;
import com.example.appsport.iBridgeFragments;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class FragmentAllExer extends Fragment {

    AdaptorRutinas adaptorRutinas;
    RecyclerView recyclerView;
    ArrayList<Rutinas>  list_rutinas;

    Activity activity;
    iBridgeFragments bridgeFragments;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_exer, container, false);
        recyclerView = view.findViewById(R.id.recycleViewRutinas);
        list_rutinas = new ArrayList<>();

        LoadDate();

       //ShowDates();
        return view;
    }

    private void LoadDate() {
        getAllExercicios(getContext());
    }

    private void ShowDates(final ArrayList<Rutinas>  list_rutinas) {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adaptorRutinas = new AdaptorRutinas(getContext(),list_rutinas);
        recyclerView.setAdapter(adaptorRutinas);

        adaptorRutinas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = list_rutinas.get(recyclerView.getChildAdapterPosition(v)).getTitle();
                Toast.makeText(getContext(),"Selected item "+title,Toast.LENGTH_SHORT).show();

               bridgeFragments.Exercices(list_rutinas.get(recyclerView.getChildAdapterPosition(v)));
            }
        });

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof Activity){
            this.activity = (Activity) context;
            bridgeFragments =(iBridgeFragments) this.activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void getAllExercicios(final Context context){
        String URL = "http://www.powerplayall.com/Android/rutinas.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(!response.isEmpty()){
                    try {
                        getArrayFromJson( new JSONArray(response.trim()),context);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Log.e("Exercicios:" , "Nothing");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return null;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }


    private  void getArrayFromJson(JSONArray json,Context context){
        ArrayList<Rutinas> list_rut = new ArrayList<>();
        for(int i=0;i<json.length();i++){
            try {
                JSONObject obj = json.getJSONObject(i);
                list_rut.add( new Rutinas(
                        obj.getInt("id_rutina"),
                        obj.getString("title")
                ));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        ShowDates(list_rut);
    }


}