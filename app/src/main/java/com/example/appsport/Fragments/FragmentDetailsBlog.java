package com.example.appsport.Fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.appsport.Model.Blogs;
import com.example.appsport.Model.Exercises;
import com.example.appsport.R;
import com.example.appsport.Utils.DownloadImageTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class FragmentDetailsBlog extends Fragment {
    TextView title,date,content;
    ImageView img;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detail_blogs,container,false);

        //Instance of variable
        title = view.findViewById(R.id.title_blog_detail);
        date = view.findViewById(R.id.date_blog_detail);
        content = view.findViewById(R.id.content_blog_details);
        img = view.findViewById(R.id.img_blog_details);

        //create new object Bundle received
        Bundle objectBlog = getArguments();
        Blogs blog = null;

        //Validation if exists object
        if(objectBlog!=null) {
            blog = (Blogs) objectBlog.getSerializable("objectBlog");

            //Show the dates in the view
            title.setText(blog.getTitle());
            date.setText("14-12-2021");
            content.setText(blog.getContent());

            String imgUrl = blog.getImg_id();

            new DownloadImageTask(img).execute(blog.getImg_id());

        }


        return view;
    }

}


