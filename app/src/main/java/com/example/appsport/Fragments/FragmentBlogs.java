package com.example.appsport.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.appsport.Adaptors.AdaptorBlogs;
import com.example.appsport.Adaptors.AdaptorExer;
import com.example.appsport.Model.Blogs;
import com.example.appsport.Model.Exercises;
import com.example.appsport.R;
import com.example.appsport.iBridgeFragments;

import java.util.ArrayList;


public class FragmentBlogs extends Fragment {

    //static AdaptorBlogs adaptorBlogs;
    static RecyclerView recyclerView;
    //ArrayList<Blogs> list_blogs;

    //Reference to bridge fragments
    Activity activity;
    static iBridgeFragments bridgeFragments;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_blogs, container, false);
        recyclerView = view.findViewById(R.id.recycleViewBlogs);
        //list_blogs = new ArrayList<>();

        //Load List
        LoadDates();
        //Show the dates
        //ShowDates();

        return view;
    }


    public void LoadDates(){
        Blogs.getAllBlogs(getContext());
    }


    public static void ShowDates(final ArrayList<Blogs> list_blogs, final Context context){
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        AdaptorBlogs adaptorBlogs = new AdaptorBlogs(context,list_blogs);
        recyclerView.setAdapter(adaptorBlogs);

        adaptorBlogs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = list_blogs.get(recyclerView.getChildAdapterPosition(view)).getTitle();
                Toast.makeText(context, "You Selected "+name , Toast.LENGTH_SHORT).show();
                bridgeFragments.sendBlogs(list_blogs.get(recyclerView.getChildAdapterPosition(view)));
            }
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof Activity){
            this.activity = (Activity) context;
            bridgeFragments =(iBridgeFragments) this.activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}