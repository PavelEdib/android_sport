package com.example.appsport.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.example.appsport.Adaptors.AdaptorExer;
import com.example.appsport.Model.Exercises;
import com.example.appsport.R;
import com.example.appsport.iBridgeFragments;

import java.util.ArrayList;


public class FragmentMyExer extends Fragment {

    private Integer id_rutina = null;
    static AdaptorExer adaptorExer;
    static RecyclerView recyclerView;
    ArrayList<Exercises> listExer;

    //Reference to bridge fragments
    Activity activity;
    static iBridgeFragments bridgeFragments;

    public FragmentMyExer(Integer id_rutina){
        this.id_rutina = id_rutina;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

            View view = inflater.inflate(R.layout.fragment_my_exer, container, false);
            recyclerView = view.findViewById(R.id.recycleView);
            listExer = new ArrayList<>();
            
            //Load List
            LoadDates();

            //Show the dates
            //ShowDates();

            return view;
    }

    //Load Dates in our ArrayList
    public void LoadDates(){
        if(this.id_rutina ==null) {
            Exercises.getAllExercicios(getContext());
        }else{
            Exercises.exerById(this.id_rutina,getContext());

        }
    }

    //Load the dates in rycyclerView
    public static void ShowDates(final ArrayList<Exercises> listExer, final Context context){
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        adaptorExer = new AdaptorExer(context,listExer);
        recyclerView.setAdapter(adaptorExer);

        //Buton de cada exercicio
        adaptorExer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = listExer.get(recyclerView.getChildAdapterPosition(view)).getName();

                //Pasamos un objeto de tipo exercicio
                bridgeFragments.sendExer(listExer.get(recyclerView.getChildAdapterPosition(view)));
            }
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            this.activity = (Activity) context;
            bridgeFragments =(iBridgeFragments) this.activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}